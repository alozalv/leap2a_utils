# Leap2A utils #

> ### Disclaimer: WIP ###
>
> The code in this repo is continous **work in progress** to learn about LEAP2A and its usage to import portfolios into **Mahara**.
> Feel free to use some of it, but take always the **master** branch as a reference.
>
> ***Leave your questions and feedback as new [issues](https://bitbucket.org/alozalv/leap2a_utils/issues).***
>
> *** Add this repo as a [git submodule](https://bitbucket.org/alozalv/leap2a_utils/wiki/How%20to%20add%20this%20repo%20as%20a%20git%20submodule) in your project ***
>
> Note that [lxml](http://lxml.de/) needs to be installed in your system.
>
> Thanks!

This repository contains a number of modules to handle Leap2A packages.

Inside the *leap2a_utils* folder, you'll find:

+ *leap2a_manager.py*: LEAP2A Manager class. Made runnable for examplary purposes (see below).
+ *config*: configuration folder. Copy the samples and replace the values on them with your own.
+ *utils*: some utility modules such as constants.

You can run *leap2a_manager.py* in order create a LEAP2A package with a single file (*--no-mahara-page*) or embed it into a [Mahara page](http://manual.mahara.org/en/1.8/portfolio/pages.html) (*--mahara-page*).

Note that this module intends to generate valid LEAP2A packages for [Mahara](https://mahara.org/). This means that the generated *leap2a.xml* file might not be fully compatible with the standard, but it will be valid from Mahara's perspective. Future versions of this module will ensure full compatibility.

```
>> ./leap2a_manager.py -h  
usage: leap2a_manager.py [-h] [-c INIFILE] [-a AUTHOR_INIFILE]
                         [-f FILEINFO_INIFILE] [-p PAGEINFO_INIFILE]
                         [-o OUTPUT_DIR] [--mahara-page] [--no-mahara-page]

Creates a LEAP2A package and embeds it into a Mahara Page if required. Check:
http://www.leapspecs.org/2A/

optional arguments:
  -h, --help            show this help message and exit
  -c INIFILE, --configfile INIFILE
                        Configuration parameters provided inside a .ini file.
                        These parameters will be overwritten by inline
                        parameters
  -a AUTHOR_INIFILE, --authorship AUTHOR_INIFILE
                        Authorship information inside a .ini file.
  -f FILEINFO_INIFILE, --fileinfo FILEINFO_INIFILE
                        File information inside a .ini file
  -p PAGEINFO_INIFILE, --pageinfo PAGEINFO_INIFILE
                        Mahara page information inside a .ini file
  -o OUTPUT_DIR, --output OUTPUT_DIR
                        Path to the directory where the LEAP2A package will be
                        stored
  --mahara-page
  --no-mahara-page

```


## Create the LEAP2A package ##

Choose option *--mahara-page* in order to embed the provided file into a Mahara page.


```
./leap2a_manager.py -c config/config.ini -a config/authorship.ini -f config/fileinfo.ini -p config/pageinfo.ini --mahara-page
== DONE! ==

```

Additional info is traced using *logging*. Provide a path of choice in your configuration file to do so.
