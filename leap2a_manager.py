#!/usr/bin/env python2.7

'''
This module handles creating a LEAP2A package.
Check: http://www.leapspecs.org/2A/


'''
import argparse, sys, ConfigParser, logging, os, shutil, zipfile, cgi
from lxml import etree as ET
from datetime import datetime

from  utils.constants import LOG_FILE, LOG_FORMAT, SECTION_GENERAL, AUTHOR_INIFILE, AUTHORSHIP,\
                INIFILE, OUTPUT_DIR, LEAP2A_XML_FILENAME, FILEINFO_INIFILE, FILE_PATH, FILE_NAME,\
                FILE_TYPE, FILEINFO, LEAP2A_ZIP_FILENAME, MAHARA_PAGE, PAGEINFO, PAGE_TITLE,\
                PAGE_SUMMARY, PAGEINFO_INIFILE

from utils.leap2a_constants import FEED, AUTHOR, NAME, EMAIL

from utils.leap2a_constants import LINK, ENTRY, TITLE, ENCLOSURE, REL, TYPE, HREF, FILES,\
                ID, ARTEFACT

from utils.leap2a_constants import VIEW, SUMMARY, RESOURCE, SELECTION,\
                CATEGORY, TERM, WEBPAGE, SCHEME, SELECTION_TYPE

from utils.leap2a_constants import ARTEFACTIDS, BLOCKINSTANCE, BLOCKTYPE, \
                BLOCKTITLE, FILEDOWNLOAD, COLUMN, ROW, LAYOUT, LAYOUT_33, \
                OWNERFORMAT, FORMAT_6, VERSION, LEAP2A_VERSION

from utils.leap2a_constants import ATOM_NS, RDF, RDF_NS, MAHARA, MAHARA_NS, LEAP2, LEAP2_NS,\
                CATEGORIES, CATEGORIES_NS, PORTFOLIO, PORTFOLIO_NS

class Leap2aManager(object):
    '''
    This class handles LEAP2A packages
    '''

    def __init__(self, initparams):
        logging.basicConfig(
            filename=initparams[LOG_FILE],
            level=logging.DEBUG,
            format=LOG_FORMAT)

    def create_feed_element(self):
        ns_map = {
            None : ATOM_NS,
            RDF : RDF_NS,
            MAHARA : MAHARA_NS,
            LEAP2 : LEAP2_NS,
            CATEGORIES : CATEGORIES_NS,
            PORTFOLIO : PORTFOLIO_NS
        }

        feed = ET.Element("{"+ATOM_NS+"}"+FEED, nsmap=ns_map)

        version = ET.Element("{"+LEAP2_NS+"}"+VERSION)
        version.text = LEAP2A_VERSION
        feed.append(version)
        
        return feed

    def create_author_element(self, author_name, author_email):
        author = ET.Element(AUTHOR)

        name = ET.Element(NAME)
        name.text = author_name
        author.append(name)

        email = ET.Element(EMAIL)
        author.append(email)  
        email.text = author_email      
        return author

    def create_link_element(self, link_rel, link_type, link_href):
        link = ET.Element(LINK)

        link.set(REL, link_rel)
        link.set(TYPE, link_type)
        link.set(HREF, link_href)

        return link

    def create_rdf_type_element(self):
        rdf_type = ET.Element("{"+RDF_NS+"}"+TYPE)

        rdf_type.set("{"+RDF_NS+"}"+RESOURCE, LEAP2+":"+SELECTION)

        return rdf_type

    def create_category_element(self):
        category = ET.Element(CATEGORY)

        category.set(TERM, WEBPAGE)
        category.set(SCHEME, CATEGORIES+":"+ SELECTION_TYPE)
        return category

    def create_mahara_view(self, block_title, artefact_id):
        mahara_artefactids = ET.Element("{"+MAHARA_NS+"}"+ARTEFACTIDS)
        mahara_artefactids.text = '[["'+artefact_id+'"]]'

        mahara_blockinstance = ET.Element("{"+MAHARA_NS+"}"+BLOCKINSTANCE)
        mahara_blockinstance.set("{"+MAHARA_NS+"}"+BLOCKTYPE, FILEDOWNLOAD)
        mahara_blockinstance.set("{"+MAHARA_NS+"}"+BLOCKTITLE, block_title)
        mahara_blockinstance.append(mahara_artefactids)

        mahara_column = ET.Element("{"+MAHARA_NS+"}"+COLUMN)
        mahara_column.append(mahara_blockinstance)

        mahara_row = ET.Element("{"+MAHARA_NS+"}"+ROW)
        mahara_row.append(mahara_column)

        view = ET.Element("{"+MAHARA_NS+"}"+VIEW)
        view.set("{"+MAHARA_NS+"}"+LAYOUT, LAYOUT_33)
        view.set("{"+MAHARA_NS+"}"+TYPE, PORTFOLIO)
        view.set("{"+MAHARA_NS+"}"+OWNERFORMAT, FORMAT_6)
        
        view.append(mahara_row)
        
        return view

    def create_entry_element(self, entry_title):
        entry = ET.Element(ENTRY)

        title = ET.Element(TITLE)
        title.text = entry_title.encode('ascii', 'ignore')
        entry.append(title)

        return entry

    def create_file_entry_element(self, entry_title, link_rel, link_type, link_href):
        entry = self.create_entry_element(entry_title)

        entry_id = ET.Element(ID)
        entry_id.text = PORTFOLIO+":"+ARTEFACT+datetime.utcnow().strftime('%Y%m%d%H%M%S')
        entry.append(entry_id)

        link = self.create_link_element(link_rel, link_type, link_href)
        entry.append(link)

        return entry

    def create_page_entry_element(self, entry_title):
        entry = self.create_entry_element(entry_title)


        entry_id = ET.Element(ID)
        entry_id.text = PORTFOLIO+":"+VIEW+datetime.utcnow().strftime('%Y%m%d%H%M%S')
        #entry_id.text = ET.QName(PORTFOLIO_NS, VIEW+datetime.utcnow().strftime('%Y%m%d%H%M%S'))
        
        entry.append(entry_id)

        return entry

    def add_pdf_file(self, file_info):
        file_href = os.path.join(FILES, file_info[FILE_NAME])
        return self.create_file_entry_element(
            file_info[FILE_NAME], ENCLOSURE, file_info[FILE_TYPE], file_href)


    def add_mahara_page(self, page_info, artefact_id):
        entry = self.create_page_entry_element(page_info[PAGE_TITLE])

        summary = ET.Element(SUMMARY)
        summary.text = page_info[PAGE_SUMMARY]
        entry.append(summary)

        rdf_type = self.create_rdf_type_element()
        entry.append(rdf_type)

        category = self.create_category_element()
        entry.append(category)

        mahara_view = self.create_mahara_view(page_info[PAGE_TITLE], artefact_id)
        entry.append(mahara_view)

        return entry

    def save_xml_file(self, root, output_dir):
        '''
        Save the resulting LEAP2A content into a file
        '''
        output_path = os.path.join(output_dir, LEAP2A_XML_FILENAME)

        tree = ET.ElementTree(root)
        tree.write(output_path, pretty_print=True, xml_declaration=True, encoding="utf-8")


    def create_xml_file(self, authorship, file_info, output_dir, embed_in_mahara_page=False, page_info={}):
        '''
        Create LEAP2A XML file
        '''
        logging.info("Creating XML file...")
        root = self.create_feed_element()

        ## Add authorship
        author = self.create_author_element(authorship[NAME], authorship[EMAIL])
        root.append(author)

        # Add file definition
        entry = self.add_pdf_file(file_info)
        root.append(entry)

        if embed_in_mahara_page:
            artefact_id = entry.find(ID).text
            entry = self.add_mahara_page(page_info, artefact_id)
            root.append(entry)

        self.save_xml_file(root, output_dir)

    def create_files_folder(self, output_dir, file_path):
        logging.info("Creating files folder......")
        
        # Create files folder
        ouput_folder = os.path.join(output_dir, FILES)
        if not os.path.exists(ouput_folder):
            os.makedirs(ouput_folder)
        
        # Move file into folder
        dst_path = os.path.join(ouput_folder, os.path.basename(file_path))
        shutil.copy(file_path, dst_path)

    def zipdir(self, path, ziph):
        dirname = os.path.split(os.path.abspath(path))[1]
        for root, dirs, files in os.walk(path):
            for zip_this in files:
                ziph.write(os.path.join(root, zip_this), os.path.join(dirname, zip_this))

    
    def compress_package(self, output_dir, zipfilename):
        logging.info("Compress package.........")

        zipf = zipfile.ZipFile(os.path.join(output_dir, zipfilename), 'w')
        # Add XML file
        zipf.write(os.path.join(output_dir, LEAP2A_XML_FILENAME), LEAP2A_XML_FILENAME)
        # Add files folder
        self.zipdir(os.path.join(output_dir, FILES), zipf)
        zipf.close()
    

    def leap2a_package(self, authorship, fileinfo, output_dir, zipfilename = LEAP2A_ZIP_FILENAME, embed_in_mahara_page=False, page_info={}):
        '''
        Create LEAP2A package
        '''

        self.create_xml_file(authorship, fileinfo, output_dir, embed_in_mahara_page, page_info)
        self.create_files_folder(output_dir, fileinfo[FILE_PATH])
        self.compress_package(output_dir, zipfilename)


def parse_config_file(inifile):
    '''
    Parse .ini configuration file at inifile
    '''
    result = {}
    try:
        cfgparser = ConfigParser.ConfigParser()
        cfgparser.read(inifile)
        result = {
            LOG_FILE:
            cfgparser.get(SECTION_GENERAL, LOG_FILE),
            OUTPUT_DIR:
            cfgparser.get(SECTION_GENERAL, OUTPUT_DIR)
            }
    except Exception:
        raise IOError("Unable to read [{0}].\
            Check that the configuration file exists and has a valid format".format(inifile))
    return result


def replace_defaults_with_arguments(defaults, arguments):
    '''
    Taking the dictionary defaults as reference,
    replace (or add if necessary) any value for the matching keys in arguments
    '''    
    params = defaults
    for param in arguments:
        if arguments[param] is not None:
            params[param] = arguments[param]

    return params


def parse_arguments():
    '''
    This function parses command line parameters
    and load all the necessary information from a config file
    '''

    parser = argparse.ArgumentParser(description=\
        "Creates a LEAP2A package\
        and embeds it into a Mahara Page if required.\
        Check: http://www.leapspecs.org/2A/")

    parser.add_argument('-c', '--configfile', action="store",
                        dest=INIFILE,
                        help="Configuration parameters provided inside a .ini file. \
        These parameters will be overwritten by inline parameters")
    parser.add_argument('-a', '--authorship', action="store",
                        dest=AUTHOR_INIFILE,
                        help="Authorship information inside a .ini file.")
    parser.add_argument('-f', '--fileinfo', action="store",
                        dest=FILEINFO_INIFILE,
                        help="File information inside a .ini file")
    parser.add_argument('-p', '--pageinfo', action="store",
                        dest=PAGEINFO_INIFILE,
                        help="Mahara page information inside a .ini file")
    parser.add_argument('-o', '--output', action="store",
                        dest=OUTPUT_DIR,
                        help="Path to the directory where the LEAP2A package will be stored")
    parser.add_argument('--mahara-page', dest='mahara_page', action='store_true')
    parser.add_argument('--no-mahara-page', dest='mahara_page', action='store_false')
    parser.set_defaults(mahara_page=True)
   
    args = parser.parse_args(sys.argv[1:])
    
    cfgparams = {}
    if args.inifile is not None:
        # Load the defaults in the .ini file
        cfgparams = parse_config_file(args.inifile)

    # Replace defaults with the parameters provided in the command line
    parameters = replace_defaults_with_arguments(cfgparams, vars(args))

    return parameters


def parse_authorship(authorship_path):
    '''
    Parse .ini authorship file at authorship_path
    '''
    authorship = {}
    try:
        cfgparser = ConfigParser.ConfigParser()
        cfgparser.read(authorship_path)
        authorship = {
            NAME:
            cfgparser.get(AUTHORSHIP, NAME),
            EMAIL:
            cfgparser.get(AUTHORSHIP, EMAIL)
            }
    except Exception:
        raise IOError("Unable to read [{0}].\
            Check that the authorship file exists and has a valid format".format(authorship_path))
    return authorship

def parse_fileinfo(fileinfo_path):
    '''
    Parse .ini fileinfo file at fileinfo_path
    '''
    fileinfo = {}
    try:
        cfgparser = ConfigParser.ConfigParser()
        cfgparser.read(fileinfo_path)
        fileinfo = {
            FILE_PATH:
            cfgparser.get(FILEINFO, FILE_PATH),
            FILE_TYPE:
            cfgparser.get(FILEINFO, FILE_TYPE)
            }
    except Exception:
        raise IOError("Unable to read [{0}].\
            Check that the fileinfo file exists and has a valid format".format(fileinfo_path))
    return fileinfo

def parse_pageinfo(pageinfo_path):
    '''
    Parse .ini pageinfo file at pageinfo_path
    '''
    pageinfo = {}
    try:
        cfgparser = ConfigParser.ConfigParser()
        cfgparser.read(pageinfo_path)
        pageinfo = {
            PAGE_TITLE:
            cfgparser.get(PAGEINFO, PAGE_TITLE),
            PAGE_SUMMARY:
            cfgparser.get(PAGEINFO, PAGE_SUMMARY)
            }
    except Exception:
        raise IOError("Unable to read [{0}].\
            Check that the pageinfo file exists and has a valid format".format(pageinfo_path))
    return pageinfo


if __name__ == '__main__':
    ARGS = parse_arguments()

    MANAGER = Leap2aManager(ARGS)
    AUTHORSHIP_PARAMS = parse_authorship(ARGS[AUTHOR_INIFILE])
    FILEINFO_PARAMS = parse_fileinfo(ARGS[FILEINFO_INIFILE])  
    PAGEINFO_PARAMS = {}
    if ARGS[MAHARA_PAGE]:
        PAGEINFO_PARAMS = parse_pageinfo(ARGS[PAGEINFO_INIFILE])
    FILEINFO_PARAMS[FILE_NAME] = os.path.basename(FILEINFO_PARAMS[FILE_PATH])  
    
    MANAGER.leap2a_package(
        AUTHORSHIP_PARAMS,
        FILEINFO_PARAMS,
        ARGS[OUTPUT_DIR],
        zipfilename=LEAP2A_ZIP_FILENAME,
        embed_in_mahara_page=ARGS[MAHARA_PAGE],
        page_info=PAGEINFO_PARAMS)

    print "== DONE! =="


