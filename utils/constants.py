'''
LEAP2A Manager Constants
'''

LOG_FILE = "log_file"
LOG_FORMAT = '[%(asctime)s] %(levelname)s [%(funcName)s:%(lineno)s] %(message)s'

INIFILE = "inifile"
SECTION_GENERAL = "general"
SECTION_LEAP2A = "leap2a"

AUTHOR_INIFILE = "author_inifile"
AUTHORSHIP = "authorship"

MAHARA_PAGE = "mahara_page"
OUTPUT_DIR = "output_dir"

LEAP2A_XML_FILENAME = "leap2a.xml"
LEAP2A_ZIP_FILENAME = "leap2a.zip"


FILEINFO_INIFILE = "fileinfo_inifile"
FILEINFO = "fileinfo"
FILE_PATH = "file_path"
FILE_NAME = "file_name"
FILE_TYPE = "file_type"


PAGEINFO_INIFILE = "pageinfo_inifile"
PAGEINFO = "pageinfo"
PAGE_TITLE = "page_title"
PAGE_SUMMARY = "page_summary"

